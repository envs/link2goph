package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
)

type link struct {
	ts    string
	link  string
	title string
}

type tree map[string][]link

const (
	ver string = "0.0.1"
)

var t tree = make(tree)
var rootFilePath string
var gopherRoot string
var host string
var port string
var user string
var subfolder bool

var dataPath string = "/.linkulator/linkulator.data"
var categories []string = make([]string, 0, 10)

//-- Find linkulator files --//

func findFiles() []string {
	fmt.Println("Finding linkulator data files")
	dataFiles := make([]string, 0, 10)
	files, err := os.Open("/home/")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error reading from filesystem: %s\n", err.Error())
		os.Exit(1)
	}
	defer files.Close()
	fileList, err := files.Readdir(0)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error reading filesystem directories: %s\n", err.Error())
		os.Exit(1)
	}
	for _, file := range fileList {
		if file.IsDir() {
			fp := fmt.Sprintf("/home/%s%s", file.Name(), dataPath)
			if _, err := os.Stat(fp); err == nil {
				dataFiles = append(dataFiles, fp)
			}
		}
	}
	return dataFiles
}

func findUserFile() []string {
	fmt.Printf("Finging %s's user data file\n", user)
	fp := fmt.Sprintf("/home/%s%s", user, dataPath)
	if _, err := os.Stat(fp); err != nil {
		fmt.Printf("User %q does not have a valid linkulator data file", user)
		os.Exit(1)
	}

	return []string{fp}
}

//-- build gopher data --//
func getCategories() []string {
	fmt.Println("Gathering category information")
	cats := make([]string, 0, len(t))
	for k, _ := range t {
		cats = append(cats, k)
	}
	sort.Strings(cats)
	return cats
}

func createGopherCategories() {
	fmt.Println("Creating category page")
	if subfolder {
		rootFilePath = filepath.Join(rootFilePath, "linkulator")
		err := os.MkdirAll(rootFilePath, 0755)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error creating subfolder: %s\n", err.Error())
			os.Exit(1)
		}
		gopherRoot = filepath.Join(gopherRoot, "linkulator")

	}
	var cats strings.Builder
	cats.WriteString("iLinkulator\tfalse\tnull.host\t70\n")
	cats.WriteString("i \tfalse\tnull.host\t70\n")
	if user == "" {
		cats.WriteString(fmt.Sprintf("iLinks gathered from the users of: %s\tfalse\tnull.host\t70\n", host))
	} else {
		cats.WriteString(fmt.Sprintf("iLinks gathered from %s's links in the %s linkulator\tfalse\tnull.host\t70\n", user, host))
	}
	cats.WriteString("i \tfalse\tnull.host\t70\n")
	cats.WriteString("i \tfalse\tnull.host\t70\n")
	cats.WriteString("iCategories\tfalse\tnull.host\t70\n")
	cats.WriteString("i---------- \tfalse\tnull.host\t70\n")
	cats.WriteString("i \tfalse\tnull.host\t70\n")
	for _, v := range getCategories() {
		gfp := filepath.Join(gopherRoot, v)
		gfp = strings.Replace(gfp, " ", "_", -1)
		ln := fmt.Sprintf("1%s\t%s\t%s\t%s\n", v, gfp, host, port)
		cats.WriteString(ln)
	}
	cats.WriteString("i \tfalse\tnull.host\t70\n")

	fp := filepath.Join(rootFilePath, "gophermap")
	err := ioutil.WriteFile(fp, []byte(cats.String()), 0644)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	err = os.Chmod(fp, 0644)
	if err != nil {
		fmt.Printf("Error setting file permissions for: %q; %s", fp, err.Error())
	}
}

func parseGopherUrl(u string) (map[string]string, error) {
	re := regexp.MustCompile(`^((?P<scheme>[a-zA-Z]+):\/\/)?(?P<host>[\w\-\.\d]+)(?::(?P<port>\d+)?)?(?:/(?P<type>[01345679gIhisp])?)?(?P<resource>.*)?$`)
	match := re.FindStringSubmatch(u)

	if valid := re.MatchString(u); !valid {
		return make(map[string]string), fmt.Errorf("Invalid link")
	}

	out := make(map[string]string)

	for i, name := range re.SubexpNames() {
		switch name {
			case "scheme":
				out["scheme"] = match[i]
			case "host":
				out["host"] = match[i]
			case "port":
				out["port"] = match[i]
			case "type":
				out["type"] = match[i]
			case "resource":
				out["resource"] = match[i]
		}
	}
	if out["port"] == "" {
		out["port"] = "70"
	}
	return out, nil
}

func createLinkListings() {
	fmt.Println("Creating individual link pages")
	for key, val := range t {
		var pg strings.Builder
		pg.WriteString(fmt.Sprintf("1Back to categories\t%s\t%s\t%s\n", gopherRoot, host, port))
		pg.WriteString("i \tfalse\tnull.host\t70\n")
		pg.WriteString("iLinkulator\tfalse\tnull.host\t70\n")
		pg.WriteString("i \tfalse\tnull.host\t70\n")
		pg.WriteString("i \tfalse\tnull.host\t70\n")
		pg.WriteString(fmt.Sprintf("iCategory %q (newest->oldest):\tfalse\tnull.host\t70\n", key))
		pg.WriteString("i \tfalse\tnull.host\t70\n")
		sort.Slice(t[key], func(i, j int) bool { return t[key][i].ts > t[key][j].ts })
		for _, l := range val {
			if strings.HasPrefix(l.link, "gopher://") {
				glink, err := parseGopherUrl(l.link)
				if err != nil {
					continue
				}
				pg.WriteString(fmt.Sprintf("%s%s\t%s\t%s\t%s\n", glink["type"], l.title, glink["resource"], glink["host"], glink["port"]))
			} else {
				pg.WriteString(fmt.Sprintf("h%s\tURL:%s\t%s\t%s\n", l.title, l.link, host, port))
			}
		}
		fp := filepath.Join(rootFilePath, key)
		fp = strings.Replace(fp, " ", "_", -1)
		err := ioutil.WriteFile(fp, []byte(pg.String()), 0644)
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		err = os.Chmod(fp, 0644)
		if err != nil {
			fmt.Printf("Error setting file permissions for: %q; %s", fp, err.Error())
		}
	}
}

//-- parse linkulator files --//

func parseLine(l string) {
	l = strings.Replace(l, "\r", "\n", -1)
	ls := strings.Split(l, "|")
	if len(ls) != 5 {
		return
	}
	if ls[1] != "" || ls[2] == "" || ls[0] == "" {
		return
	}
	if _, ok := t[ls[2]]; !ok {
		t[ls[2]] = make([]link, 0, 10)
	}
	ls[0] = strings.TrimSpace(ls[0])
	ls[2] = strings.TrimSpace(ls[2])
	ls[3] = strings.TrimSpace(ls[3])
	ls[4] = strings.TrimSpace(ls[4])
	t[ls[2]] = append(t[ls[2]], link{ls[0], ls[3], ls[4]})
}

func parseFiles() {
	var fileList []string
	if user == "" {
		fileList = findFiles()
	} else {
		fileList = findUserFile()
	}
	fmt.Println("Parsing data files")
	for _, file := range fileList {
		f, err := os.Open(file)
		if err != nil {
			fmt.Printf("Unable to open file: %q", file)
			continue
		}
		r := bufio.NewReader(f)
		for {
			ln, _, err := r.ReadLine()
			if err != nil {
				break
			}
			parseLine(string(ln))
		}
		f.Close()
	}
}

func printHelp() {
	art := `link2goph - linkulator to gopher
Syntax: link2goph [flag [flag value]] ...

Example: link2goph -r ~/gopher/ -s -n hostname.com
         link2goph -v

Options:
`
	fmt.Fprint(os.Stdout, art)
	flag.PrintDefaults()
}

func main() {
	flag.StringVar(&rootFilePath, "r", "~", "Set the root folder for output (should be writable), defaults to: ~")
	flag.StringVar(&gopherRoot, "g", "/", "Set the root gopher path for output, defaults to: /")
	flag.StringVar(&host, "n", "null.host", "Set the gopher host name, defaults to: null.host")
	flag.StringVar(&port, "p", "70", "Set the gopher port, defaults to: 70")
	flag.StringVar(&user, "u", "", "Export only links by the given username, defaults to \"\"")
	flag.BoolVar(&subfolder, "s", false, "If present, a subfolder will be created to house the gopher output")
	version := flag.Bool("v", false, "Display version information and exit")
	flag.Usage = printHelp
	flag.Parse()
	if *version {
		fmt.Println(ver)
		os.Exit(0)
	}
	parseFiles()
	createGopherCategories()
	createLinkListings()
	fmt.Printf("-- Created files in %s --\n", rootFilePath)
}
