# link2goph

Created a gopher listing of the categories and their links for a linkulator isntance (using the linkulator2 format). The output gophermaps will not list the users that posted the links and do not include comments. This is intentional, as that conversation and identification was meant for the audience on the server... not necessarily public consumption. Listing only the links lets people discover cool things, while staying away from publicizing comments and identifying information. Hopefully this is deemed to be in keeping with the style and format of linkulator.

## Building

link2goph only uses the Go standard library and should be compatible with at least 1.12 forward (possibly 1.11, I have not tested it yet).

To build, simply run: `go build` or `go install` in accordance with your preference from the root directory of the project.

## Running

If you used `go install` then link2goph should hopefully be on your path. In which case you can run it like so:

```shell
link2goph -s -r ./ -p 70 -h agopher.hole -g /~sloum
```

The following options are available:

- -g gopher folder, pass the _gopher_ selector root for where you will refer to the files (defaults to: /)
- -n host name, pass in your host name (defaults to: null.host)
- -p port, pass in the port you will be serving things on (defaults to 70)
- -r root folder, pass in the _system_ path for where you want the files (defaults to your home directory)
- -s creates a linkulator folder in the root and gopher folders you specify and places the files in it and adjust the links to suit that path
- -u user, pass in a username to have the output consist of links that belong to that user
- -v print version info and exit

## Etc

Using the `-u` flag _does_ include the user's name in the output header. I mention it here since it goes against the above statement regarding keeping user names and comment data out of the generated gopher content.
